<?php
    require_once('animal.php');
    require_once('ape.php');
    require_once('frog.php');

    $sheep = new Animal("shaun");
    echo "nama hewan = ". $sheep->name. "<br>";
    echo "jumlah kaki = ". $sheep->legs. "<br>";
    echo "cold blooded = ". $sheep->coldblooded. "<br><br>";
    
    $sungokong = new  Ape("kerasakti");
    echo "nama hewan = ". $sungokong->name. "<br>";
    echo "jumlah kaki = ". $sungokong->legs. "<br>";
    echo "cold blooded = ". $sungokong->coldblooded. "<br>";
    echo  $sungokong->yell("auoooo"). "<br><br>";
    
    $kodok = new Frog("Buduk");
    echo "nama hewan = ". $kodok->name. "<br>";
    echo "jumlah kaki = ". $kodok->legs. "<br>";
    echo "cold blooded = ". $kodok->coldblooded. "<br>";
    

?> 